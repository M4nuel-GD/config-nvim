require'nvim-treesitter.configs'.setup {
	-- A list of parser names, or "all"
	ensure_installed = { "c", "lua", "php", "bash", "css" },

	-- Install parsers synchronously (only applied to `ensure_installed`)
	sync_install = false,

	-- List of parsers to ignore installing (for "all")
	ignore_install = { "javascript" },

	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},
	context_commentstring = {
		enable = true,
		enable_autocmd = false,
	},
	rainbow = {
		enable = true,
		disable = { "html" },
		extended_mode = false,
		max_file_lines = nil,
	},
	autotag = { enable = true },
	incremental_selection = { enable = true },
	indent = { enable = true },
	textobjects = {
		select = {
			enable = true,
			lookahead = true,
			keymaps = {
				["af"] = "@function.outer",
				["if"] = "@function.inner",
				["ai"] = "@conditional.outer",
				["ii"] = "@conditional.inner",
				["al"] = "@loop.outer",
				["il"] = "@loop.inner",
				['aa'] = '@parameter.outer',
				['ia'] = '@parameter.inner',
				['ac'] = '@class.outer',
				['ic'] = '@class.inner',
				['at'] = '@comment.outer',
				['ir'] = '@return.inner',
				['ar'] = '@return.outer',
				["ae"] = { query = "@assignment.outer", desc = "Select outer part of an assignment" },
				["ie"] = { query = "@assignment.inner", desc = "Select inner part of an assignment" },
				["le"] = { query = "@assignment.lhs", desc = "Select left hand side of an assignment" },
				["re"] = { query = "@assignment.rhs", desc = "Select right hand side of an assignment" },
			},
		},
	},
}
