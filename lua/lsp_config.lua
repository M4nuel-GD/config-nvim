local lspconfig = require('lspconfig')
-- local signs = {
-- 	{ name = "DiagnosticSignError", text = " " },
-- 	{ name = "DiagnosticSignWarn", text = " " },
-- 	{ name = "DiagnosticSignHint", text = " " },
-- 	{ name = "DiagnosticSignInfo", text = " " },
-- }

-- for _, sign in ipairs(signs) do
-- 	vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
-- end


local config = {
	-- disable virtual text
	virtual_text = true,
	-- show signs
	signs = {
		active = signs,
	},
	update_in_insert = true,
	underline = true,
	severity_sort = true,
	float = {
		focusable = false,
		style = "minimal",
		border = "rounded",
		source = "always",
		header = "",
		prefix = "",
	},
}

vim.diagnostic.config(config)

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
	border = "rounded",
})

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
	border = "rounded",
})

local function lsp_highlight_document(client)
  -- Set autocommands conditional on server_capabilities
  if client.resolved_capabilities.document_highlight then
	vim.api.nvim_exec(
		[[
		augroup lsp_document_highlight
		autocmd! * <buffer>
		autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
		autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
		augroup END
	]],
		false
	)
  end
end

local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', 'dp', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', 'dn', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<leader>D', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', '<leader>I', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('i', '<C-o>', '<C-x><C-o>', bufopts)
  --vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<leader>wl',  function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
--  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<leader>f', vim.lsp.buf.format ,bufopts)
end
--
-- vim.opt.completeopt = {'menu', 'menuone', 'noselect', 'noinsert'}
-- vim.opt.shortmess:append('c')
--
-- local function tab_complete()
-- 	if vim.fn.pumvisible() == 1 then
-- 		-- navigate to next item in completion menu
-- 		return '<Down>'
-- 	end
--
-- 	local c = vim.fn.col('.') - 1
-- 	local is_whitespace = c == 0 or vim.fn.getline('.'):sub(c, c):match('%s')
--
-- 	if is_whitespace then
-- 		-- insert tab
-- 		return '<Tab>'
-- 	end
--
-- 	local lsp_completion = vim.bo.omnifunc == 'v:lua.vim.lsp.omnifunc'
--
-- 	if lsp_completion then
-- 		-- trigger lsp code completion
-- 		return '<C-x><C-o>'
-- 	end
--
-- 	-- suggest words in current buffer
-- 	return '<C-x><C-n>'
-- end
--
-- local function tab_prev()
-- 	if vim.fn.pumvisible() == 1 then
-- 		-- navigate to previous item in completion menu
-- 		return '<Up>'
-- 	end
--
-- 	-- insert tab
-- 	return '<Tab>'
-- end
--
-- vim.keymap.set('i', '<Tab>', tab_complete, {expr = true})
-- vim.keymap.set('i', '<S-Tab>', tab_prev, {expr = true})




-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
-- local capabilities_1 = require('cmp_nvim_lsp').default_capabilities()
--local servers = { 'pyright', 'tsserver', 'bashls', 'emmet_ls', 'jdtls' }
local servers = { 'clangd', 'jdtls', 'pylsp', 'emmet_ls' }
for _, lsp in pairs(servers) do
  lspconfig[lsp].setup {
	on_attach = on_attach,
	-- capabilities = capabilities_1,
	flags = {
		-- This will be the default in neovim 0.7+
		debounce_text_changes = 150,
	}
  }
end
--
-- local runtime_path = vim.split(package.path, ';')
-- table.insert(runtime_path, "lua/?.lua")
-- table.insert(runtime_path, "lua/?/init.lua")
-- lspconfig.sumneko_lua.setup {
--   settings = {
-- 	Lua = {
-- 		runtime = {
-- 		-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
-- 		version = 'LuaJIT',
-- 		-- Setup your lua path
-- 		path = runtime_path,
-- 		},
-- 		diagnostics = {
-- 		-- Get the language server to recognize the `vim` global
-- 		globals = {'vim'},
-- 		},
-- 		workspace = {
-- 		-- Make the server aware of Neovim runtime files
-- 		library = vim.api.nvim_get_runtime_file("", true),
-- 		},
-- 		-- Do not send telemetry data containing a randomized but unique identifier
-- 		telemetry = {
-- 		enable = false,
-- 		},
-- 	},
--   },
-- }

vim.diagnostic.config({
  virtual_text = {
    prefix = '<', -- Could be '●', '▎', 'x'
  }
})
