local function lsp()
	local count = {}
	local levels = {
		errors = "Error",
		warnings = "Warn",
		info = "Info",
		hints = "Hint",
	}

	for i, level in pairs(levels) do
		count[i] = vim.tbl_count(vim.diagnostic.get(0, { severity = level }))
	end

	local errors = ""
	local warnings = ""
	local hints = ""
	local info = ""

	if count["errors"] ~= 0 then
		errors = " %#LspDiagnosticsSignError# " .. count["errors"]
	end
	if count["warnings"] ~= 0 then
		warnings = " %#LspDiagnosticsSignWarning#  " .. count["warnings"]
	end
	if count["hints"] ~= 0 then
		hints = " %#LspDiagnosticsSignHint# " .. count["hints"]
	end
	if count["info"] ~= 0 then
		info = " %#LspDiagnosticsSignInformation#  " .. count["info"]

	end

	return errors .. warnings .. hints .. info .. "%#Bar_Normal#"
end



local function filetype()
	return string.format(" %s ", vim.bo.filetype):upper()
end

local modes = {
	["n"] = "NORMAL",
	["no"] = "NORMAL",
	["v"] = "VISUAL",
	["V"] = "VISUAL LINE",
	[""] = "VISUAL BLOCK",
	["s"] = "SELECT",
	["S"] = "SELECT LINE",
	[""] = "SELECT BLOCK",
	["i"] = "INSERT",
	["ic"] = "INSERT",
	["R"] = "REPLACE",
	["Rv"] = "VISUAL REPLACE",
	["c"] = "COMMAND",
	["cv"] = "VIM EX",
	["ce"] = "EX",
	["r"] = "PROMPT",
	["rm"] = "MOAR",
	["r?"] = "CONFIRM",
	["!"] = "SHELL",
	["t"] = "TERMINAL",
}

local function mode()
	local current_mode = vim.api.nvim_get_mode().mode
	return string.format(" %s ", modes[current_mode]):upper()
end

local function update_mode_colors()
	local current_mode = vim.api.nvim_get_mode().mode
	local mode_color = "%#StatusLineAccent#"
	if current_mode == "n" then
		mode_color = "%#Block_green#"
	elseif current_mode == "i" or current_mode == "ic" then
		mode_color = "%#Block_blue#"
	elseif current_mode == "v" or current_mode == "V" or current_mode == "" then
		mode_color = "%#Block_magenta#"
	elseif current_mode == "R" then
		mode_color = "%#Block_orange#"
	elseif current_mode == "c" then
		mode_color = "%#Block_gray#"
	elseif current_mode == "t" then
		mode_color = "%#StatuslineTerminalAccent#"
	end
	return mode_color
end



Statusbar = function()
	return table.concat {
		update_mode_colors(),
		mode(),
		"%#Bar_Normal#% ",
		filetype(),
		" %f %m %=",
		lsp(),
		" %#Bar_Normal#%{&fileencoding?&fileencoding:&encoding} [%{&fileformat}]",
	}
end
