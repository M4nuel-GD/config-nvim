local cmp_status_ok, cmp = pcall(require, "cmp")
if not cmp_status_ok then
	return
end

local has_words_before = function()
	local line, col = unpack(vim.api.nvim_win_get_cursor(0))
	return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

--local feedkey = function(key, mode)
--	vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
--end
local function feedkeys(keys)
	vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(keys, true, false, true), 'n', true)
end

-- local feedkeys = vim.fn.feedkeys
-- local replace_termcodes = vim.api.nvim_replace_termcodes
-- --local backspace_keys = replace_termcodes('<tab>', true, true, true)
-- local snippet_next_keys = replace_termcodes('<Plug>(vsnip-expand-or-jump)', true, true, true)
-- local snippet_prev_keys = replace_termcodes('<Plug>(vsnip-jump-prev)', true, true, true)



local border_opts = { border = "single", winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:Visual,Search:None" }
local kind_icons = {
    Text = "",
    Method = "󰆧",
    Function = "󰊕",
    Constructor = "",
    Field = "󰇽",
    Variable = "󰂡",
    Class = "󰠱",
    Interface = "",
    Module = "",
    Property = "󰜢",
    Unit = "",
    Value = "󰎠",
    Enum = "",
    Keyword = "󰌋",
    Snippet = "",
    Color = "󰏘",
    File = "󰈙",
    Reference = "",
    Folder = "󰉋",
    EnumMember = "",
    Constant = "󰏿",
    Struct = "",
    Event = "",
    Operator = "󰆕",
    TypeParameter = "󰅲",
  }

-- find more here: https://www.nerdfonts.com/cheat-sheet

cmp.setup {
	completion = { completeopt = 'menu,menuone,noselect' },
	preselect = cmp.PreselectMode.None,
	sorting = {
	comparators = {
		cmp.config.compare.offset,
		cmp.config.compare.exact,
		cmp.config.compare.score,
		cmp.config.compare.kind,
		cmp.config.compare.sort_text,
		cmp.config.compare.length,
		cmp.config.compare.order,
	},
	},

	snippet = {
	expand = function(args)
		-- vim.fn["vsnip#anonymous"](args.body)
		vim.snippet.expand(args.body)
	end,
	},

	mapping = cmp.mapping.preset.insert({
      		['<C-b>'] = cmp.mapping.scroll_docs(-4),
		['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-l>'] = cmp.mapping.complete(),
		['<C-e>'] = cmp.mapping.abort(),
		['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
		["<C-Space>"] = cmp.mapping(function()
                          if cmp.visible() then
                                  cmp.select_next_item()
                          end
                  end, { "i", "s" }),

                  ["<C-p>"] = cmp.mapping(function()
                          if cmp.visible() then
                                  cmp.select_prev_item()
                          end
                  end, { "i", "s" }),


                  ["<Tab>"] = cmp.mapping(function(fallback)
                          if vim.snippet.active { direction = 1 } then
                                  vim.snippet.jump(1)
                          else
                                  -- fallback()
				  feedkeys '<Tab>'
                          end
                  end, { "i", "s" }),

                  ["<S-Tab>"] = cmp.mapping(function(fallback)
                          if vim.snippet.active { direction = -1 } then
                                  vim.snippet.jump(-1)
                          else
                                  -- fallback()
				  feedkeys '<S-Tab>'
                          end
                  end, { "i", "s" }),

	}),




	formatting = {
	fields = { "kind", "abbr" },
	format = function(entry, vim_item)
		-- Kind icons
		vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
		-- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
		vim_item.menu = ({
		nvim_lsp = "LSP",
		--vsnip = "Snip",
		buffer = "Buff",
		path = "Path",
		})[entry.source.name]
		return vim_item
	end,
	},
	sources = {
		{ name = "nvim_lua"},
		{ name = "nvim_lsp" },
		-- { name = "vsnip" },
		{ name = "buffer" },
		{ name = "path" }
	},
	confirm_opts = {
		behavior = cmp.ConfirmBehavior.Replace,
		select = false,
	},
	window ={
		documentation = cmp.config.window.bordered(border_opts),
	},
	duplicates = {
		nvim_lsp = 1,
		-- vsnip = 1,
		buffer = 1,
		path = 1,
	},
--	documentation = {
--		border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
--	},
		experimental = {
		ghost_text = false,
		native_menu = false,
	},
}
