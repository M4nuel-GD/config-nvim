vim.g.mapleader = " "
vim.g.netrw_banner = 0
vim.g.netrw_liststyle = 3
vim.opt.clipboard = "unnamedplus"
vim.opt.splitright = true
vim.g.splitbelow = true
vim.opt.mouse = 'a'
--Case insensitive searching UNLESS /C or capital in search
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.termguicolors = true
-- vim.opt.guicursor = "a:ver90"
vim.opt.autoindent = false

vim.opt.expandtab = false
vim.bo.expandtab = false

vim.opt.backup = false                          -- creates a backup file
vim.opt.fileencoding = "utf-8"                  -- the encoding written to a file
vim.opt.ignorecase = true                       -- ignore case in search patterns
vim.opt.pumheight = 10                          -- pop up menu height
vim.opt.smartindent = false                      -- make indenting smarter again
vim.opt.swapfile = false                        -- creates a swapfile
vim.g.swapfile = false --pepe
vim.opt.timeoutlen = 1000                       -- time to wait for a mapped sequence to complete (in milliseconds)
vim.opt.undofile = false                         -- enable persistent undo
vim.opt.writebackup = false                     -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
vim.opt.cursorline = false                       -- highlight the current line
vim.opt.signcolumn = "yes"
vim.opt.showmode = true
vim.opt.number = true
vim.opt.relativenumber = true                  -- set relative numbered lines
vim.opt.wrap = false                            -- display lines as one long line
vim.opt.updatetime = 250
vim.o.laststatus = 3
vim.o.statusline = '%!v:lua.Statusbar()'

vim.opt.fillchars = { eob = " ", }
--COLORS
--
vim.cmd([[
autocmd BufWritePre * :%s/\s\+$//e
autocmd TermOpen * setlocal nonumber norelativenumber statusline=%#Bar_Normal#\ TERMINAL\ %f
]])
