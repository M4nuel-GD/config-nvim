local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

--  Shorten function name
-- local vim.keymap.set = vim.api.nvim_set_keymap

-- Remap space as leader key
vim.keymap.set("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

--  Modes
--    normal_mode = "n",
--    insert_mode = "i",
--    visual_mode = "v",
--    visual_block_mode = "x",
--    term_mode = "t",
--    command_mode = "c",
vim.keymap.set("x", "<leader>s", ":!sort<CR>", term_opts)
vim.keymap.set("n", "<F5>", ":set invnumber<CR>", term_opts)
vim.keymap.set("n", "<F6>", ":set invrnu<CR>", term_opts)
--  Normal --
--  Better window navigation
--  vim.keymap.set("n", "<C-Up>", "<C-w>k", opts)
--  vim.keymap.set("n", "<C-Down>", "<C-w>j", opts)
--  vim.keymap.set("n", "<C-Left>", "<C-w>h", opts)
--  vim.keymap.set("n", "<C-Right>", "<C-w>l", opts)
vim.keymap.set("n", "<C-k>", "<C-w>k", opts)
vim.keymap.set("n", "<C-j>", "<C-w>j", opts)
vim.keymap.set("n", "<C-h>", "<C-w>h", opts)
vim.keymap.set("n", "<C-l>", "<C-w>l", opts)

vim.keymap.set("n", "<C-q>", ":bw<CR>", opts)

vim.keymap.set("n", "<leader>Q", ":bd<cr>", opts)
vim.keymap.set("n", "<leader>q", ":q<cr>", opts)
vim.keymap.set("n", "<leader>w", ":w<cr>", opts)
vim.keymap.set("n", "<leader>l", ":Lex 20<cr>", opts)
vim.keymap.set("n", "<leader>k", ":bn<cr>", opts)
vim.keymap.set("n", "<leader>j", ":bp<cr>", opts)
vim.keymap.set("n", "<leader>v", ":vs<cr>", opts)
vim.keymap.set("n", "<leader>s", ":sp<cr>", opts)
--  fzf
vim.keymap.set("n", "<leader>f", ":Files<cr>", opts)
vim.keymap.set("n", "<leader>g", ":GFiles<cr>", opts)
vim.keymap.set("n", "<leader>r", ":Rg<cr>", opts)
vim.keymap.set("n", "<leader>b", ":Buffers<cr>", opts)
--  vim.keymap.set("n", "<C-l>", ":Files<cr>", opts)

--  Resize with arrows
vim.keymap.set("n", "<M-k>", ":resize +2<CR>", opts)
vim.keymap.set("n", "<M-j>", ":resize -2<CR>", opts)
vim.keymap.set("n", "<M-h>", ":vertical resize -2<CR>", opts)
vim.keymap.set("n", "<M-l>", ":vertical resize +2<CR>", opts)

--  hjkl in command_mode and insert_mode

vim.keymap.set("c", "<M-k>", "<Up>")
vim.keymap.set("c", "<M-j>", "<Down>")
vim.keymap.set("c", "<M-h>", "<Left>")
vim.keymap.set("c", "<M-l>", "<Right>")

vim.keymap.set("i", "<M-k>", "<Up>")
vim.keymap.set("i", "<M-j>", "<Down>")
vim.keymap.set("i", "<M-h>", "<Left>")
vim.keymap.set("i", "<M-l>", "<Right>")

vim.keymap.set("t", "<M-k>", "<Up>")
vim.keymap.set("t", "<M-j>", "<Down>")
vim.keymap.set("t", "<M-h>", "<Left>")
vim.keymap.set("t", "<M-l>", "<Right>")

-- Autopair
-- vim.keymap.set('i', '"', '""<Esc>i')
-- vim.keymap.set('i', '(', '()<Esc>i')
-- vim.keymap.set('i', '{', '{}<Esc>i')
-- vim.keymap.set('i', '[', '[]<Esc>i')
-- vim.keymap.set('i', '<', '<><Esc>i')
-- vim.keymap.set('i', "'", "''<Esc>i")
-- -- Terminal --
-- -- Better terminal navigation
-- vim.keymap.set("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
-- vim.keymap.set("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
-- vim.keymap.set("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
-- vim.keymap.set("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)
vim.keymap.set("v", "(", "<esc>`>a)<esc>`<i(<esc>gv", { remap = false})
vim.keymap.set("v", "{", "<esc>`>a}<esc>`<i{<esc>gv", { remap = false})
vim.keymap.set("v", "[", "<esc>`>a]<esc>`<i[<esc>gv", { remap = false})
